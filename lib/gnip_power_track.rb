require 'gnip_power_track/version'
require 'net/http'
require 'buftok'

class GnipPowerTrack
  attr_reader :uri, :account

  def initialize(options = {})
    @username = options.fetch(:username)
    @password = options.fetch(:password)
    @account  = options.fetch(:account)
    @uri = URI("https://stream.gnip.com:443/accounts/#{@account}/publishers/"\
             'twitter/streams/track/prod.json')
    @tokenizer = BufferedTokenizer.new("\r\n")
  end

  def start(&_block)
    net_http.start do |http|
      http.request request do |response|
        response.read_body do |chunk|
          activities = @tokenizer.extract(chunk)
          activities.reject!(&:empty?)
          activities.each { |activity| yield(activity) }
        end
      end
    end
  end

  private

  def net_http
    return @net_http unless @net_http.nil?

    @net_http = Net::HTTP.new(@uri.host, @uri.port)
    @net_http.read_timeout = 31
    @net_http.use_ssl = true

    @net_http
  end

  def request
    @request unless @request.nil?

    @request = Net::HTTP::Get.new(@uri)
    @request.basic_auth(@username, @password)

    @request
  end
end
